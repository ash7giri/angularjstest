(function(){
var app = angular.module('myApp',['ngRoute']);
var arr=[];
app.config(['$routeProvider',function($routeProvider) {
		$routeProvider
			.when("/employees", {
				templateUrl : "views/employee.html",
				controller : "EmployeeController"
			})
			.when("/employees/add", {
				templateUrl : "views/addEmp.html",
				controller : "AddEmpController"
			})
			.when("/employees/:userId/edit", {
				templateUrl : "views/editEmp.html",
				controller : "EditEmpController"
			})
			.otherwise({
				redirectTo : '/employees'
			});
}]);
app.service('getEmployee',['$http','$q',function($http,$q) {
		var deferred=$q.defer();
		$http.get("./data/employee-info.json").success(function(response) {
			deferred.resolve(response);
		});
		this.getEmp=function(){
			return deferred.promise;
		}
		this.create= function(obj) {
			var config = {
				method: "POST",
				url: "./data/employee-info.json",
				data: obj,
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				}
			};
			return $http(config);
		}
		this.update= function(id,obj) {
			var config = {
				method: "POST",
				url: "./data/employee-info.json",
				params:id,
				data: obj,
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				}
			};
			return $http(config);
		}
		this.get= function(id) {		
		   var result = null;
      	   angular.forEach(arr, function(i) {
              if(i.id == id) result = i;
    	   });
           return result;
		}
}]);
app.filter('checkString', ['$filter', function ($filter) {
    return function (input) {
        if (isNaN(input)) {
            return 'NA';
        } else {
            return input
        };
    };
}]);
app.controller('EmployeeController',['$scope','$filter','getEmployee', function($scope,$filter,getEmployee) {
	  var promise=getEmployee.getEmp();
	  promise.then(function(data){
		  arr=data.data;
		  $scope.employeeData=arr;
	  });
	  $scope.searchFilter = function (input) {
		var filterData = new RegExp($scope.searchVal, 'i');
		return !$scope.searchVal || filterData.test(input.name) || filterData.test(input.address.city);
	  };
	  $scope.orderByField = 'name';
	  $scope.reverseSort = false;
	  $scope.sortData=function(column){
		  $scope.reverseSort=($scope.orderByField==column) ? !$scope.reverseSort :false;
		  $scope.orderByField=column;
	  }
}]);
app.controller('AddEmpController',['$scope','$location','getEmployee',function($scope,$location,getEmployee) {
	$scope.submitData=function(){
		 var data={
		  "id": $scope.detail.id,
		  "name": $scope.detail.name,
          "phone": $scope.detail.phone,
          "address":
             {
				 "city": $scope.detail.city,
				 "address_line1":$scope.detail.address1,
				 "address_line2":$scope.detail.address2,
				 "postal_code":$scope.detail.pin
			 }
		}
		getEmployee.create(data).then(function(response){
			arr.push(data);
			$location.path('/');
		});
	}
}]);
app.controller('EditEmpController',['$scope','$routeParams','getEmployee','$location', function($scope,$routeParams,getEmployee,$location) {
	  $scope.detail= getEmployee.get($routeParams.userId);
	  $scope.submitData=function(){
		 var data={
		  "id": $scope.detail.id,
		  "name": $scope.detail.name,
          "phone": $scope.detail.phone,
          "address":
             {
				 "city": $scope.detail.address.city,
				 "address_line1":$scope.detail.address.address_line1,
				 "address_line2":$scope.detail.address.address_line2,
				 "postal_code":$scope.detail.address.postal_code
			 }
		}
		getEmployee.update($routeParams.userId,data).then(function(response){
		   angular.forEach(arr, function(i) {
              if(i.id == $routeParams.userId){
				 i.id=$scope.detail.id,
				 i.name=$scope.detail.name,
				 i.phone=$scope.detail.phone,
				 i.address.city=$scope.detail.address.city,
				 i.address.address_line1=$scope.detail.address.address_line1,
				 i.address.address_line2=$scope.detail.address.address_line2,
				 i.address.postal_code=$scope.detail.address.postal_code
			  } 
    	   });
		});
		$location.path('/');
	 }
}]);
})();
